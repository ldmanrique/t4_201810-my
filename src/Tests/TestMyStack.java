package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.MyStack;

public class TestMyStack {




	private MyStack<Integer> pila;

	@Before
	public void setUpEscenario()
	{
		pila= new MyStack<Integer>();
		pila.push(2);
		pila.push(3);
		pila.push(5);
		pila.push(1);
		pila.push(7);
	}

	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	/**
	 * push
	 * pop
	 * size
	 * isEmpty
	 */
	@Test
	public void TestSize()
	{
		assertEquals("El tama�o de la pila no es el correcto", pila.size(), 5);
	}
	@Test
	public void testIsEmpty()
	{
		assertEquals("La pila no deber�a estar vac�a", pila.isEmpty(), false);
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();

		assertTrue("La pila deber�a estar vac�a", pila.isEmpty());
	}

	@Test
	public void testpush()
	{
		pila.push(9);
		assertEquals("El tama�o de la pila deber�a ser 6", pila.size(), 6);

	}
	@Test
	public void testpop()
	{
		pila.pop();
		assertEquals("El tama�o de la pila deber�a haber disinu�do en 1", pila.size(), 4);

	}





}

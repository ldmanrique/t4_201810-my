package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import api.ITaxiTripsManager;
import javafx.scene.image.Image;
import jdk.nashorn.internal.ir.CatchNode;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.MyLinkedList;
import model.data_structures.MyMaxPQ;
import model.data_structures.MyQueue;
import model.data_structures.MyStack;
import model.data_structures.Node;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;

import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import sun.print.resources.serviceui;


public class TaxiTripsManager implements ITaxiTripsManager 
{	//Taller 4
	ArrayList<Taxi> taxish= new ArrayList<Taxi>();
	ArrayList<CompaniaServicios> companis= new ArrayList<CompaniaServicios>();



	// TODO
	// Definition of data model 
	private MyLinkedList<Servicio> services = new MyLinkedList<Servicio>();

	private MyLinkedList<String> ids = new MyLinkedList<String>();
	private MyLinkedList<Compania> compa�iaServicio = new MyLinkedList<Compania>();
	private MyLinkedList<Compania> compa�ia = new MyLinkedList<Compania>();
	private MyLinkedList<ZonaServicios> zonaServicio = new MyLinkedList<ZonaServicios>();


	public static final String[] DIRECCION_LARGE_JSON ={

			"./data/taxi-trips-wrvz-psew-subset-02-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-03-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-04-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-05-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-06-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-07-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-08-02-2017.json"

	};

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";


	DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
	//Taller 4, Parte 2, punto 2

	public void loadServices(String direccionJson, RangoFechaHora rango) {
		try{
			FileInputStream stream = new FileInputStream(new File(direccionJson));
			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
			Gson gson = new GsonBuilder().create();
			// Read file in stream mode
			reader.beginArray();
			while (reader.hasNext()) {
				// Read data into object model
				Taxi srvc = gson.fromJson(reader, Taxi.class);
				Servicio s = gson.fromJson(reader, Servicio.class);
				if (!existsTaxi(srvc.gettaxi_id())) {

					Date f1= parseRangoFecha(rango.getFechaInicial(), rango.getHoraInicio());
					Date f2= parseRangoFecha(rango.getFechaFinal(), rango.getHoraFinal());
					Date f3= parseRangoFechaCompleto(s.getTrip_start_timestamp());
					Date f4= parseRangoFechaCompleto(s.getTrip_end_timestamp());

					if(f1.before(f3)&& f2.after(f4)){
						CompaniaServicios c = gson.fromJson(reader, CompaniaServicios.class);
						if(c.getcompany()==null){
							c.setcompany("Independent Owner");
							if (!existsCompany(c.getcompany())){
								c.getServicios().add(s);
								companis.add(c);
							}
						}
						if(srvc.getCompany()==null)	{
							srvc.setCompany("Independent Owner");
						}
						srvc.getServicios().add(s);
						taxish.add(srvc);
					}
				}

				break;
			}
			reader.close();
		}catch(Exception e){System.out.println("Falla la carga");}
	}



	//Comprueba si existe un taxi con la id pasada por par�metro
	private boolean existsTaxi(String id)
	{
		int tama�o = taxish.size();

		for (int i = 0 ; i < tama�o; i++)
		{
			Taxi actual = taxish.get(i);
			if (actual.gettaxi_id().equals(id))
			{
				return true;
			}
		}
		return false;
	}

	//Comprueba si existe una compa��a con el nombre pasado por par�metro
	private boolean existsCompany(String name)
	{
		int tama�o = companis.size();

		for (int i = 0 ; i < tama�o; i++)
		{
			CompaniaServicios actual = companis.get(i);
			if (actual.getcompany().equals(name))
			{
				return true;
			}
		}
		return false;
	}

	//Taller 4, Parte 2, punto 1
	public  Taxi[] heapTaxis()
	{
		Taxi[] retornar= new Taxi[taxish.size()];
		taxish.toArray(retornar);

		int i;

		for(i=retornar.length; i>1; i--)
		{
			fnSortHeap(retornar, i - 1);
		}

		return retornar;
	}

	private void fnSortHeap(Taxi arr[], int arr2)
	{
		int i, o;
		int lCh, rCh, mCh, root;
		Taxi tmp= null;
		root = (arr2-1)/2;

		for(o = root; o >= 0; o--)
		{
			for(i=root;i>=0;i--)
			{
				lCh = (2*i)+1;
				rCh = (2*i)+2;
				if((lCh <= arr2) && (rCh <= arr2))
				{
					if(arr[rCh].gettaxi_id().compareTo(arr[lCh].gettaxi_id()) >0)
					{
						mCh = rCh;
					}
					else
					{
						mCh = lCh;
					}
				}
				else
				{
					if(rCh > arr2)
					{
						mCh = lCh;
					}
					else
					{
						mCh = rCh;
					}
				}

				if(arr[i].gettaxi_id().compareTo(arr[mCh].gettaxi_id())<0)
				{
					tmp = arr[i];
					arr[i] = arr[mCh];
					arr[mCh] = tmp;
				}
			}
		}
		tmp = arr[0];
		arr[0] = arr[arr2];
		arr[arr2] = tmp;
		return;
	}


	//Taller 4, parte 2, punto 3
	public MyMaxPQ<String> empresasAmayor() {
		MyMaxPQ<String>  retornar= null;
		CompaniaServicios[] c= new CompaniaServicios[companis.size()];
		companis.toArray(c);
			int i;

		for(i=c.length; i>1; i--)
		{
			fnSortHeapCompanies(c, i - 1);
		}
		i=0;
		while(i< c.length)
		{
			retornar.insert(v);
			//Se acab� el tiempoo
			
			i++;
		}
		
		return retornar;
	}

	private void fnSortHeapCompanies(CompaniaServicios arr[], int arr2)
	{
		int i, o;
		int lCh, rCh, mCh, root;
		CompaniaServicios tmp= null;
		root = (arr2-1)/2;

		for(o = root; o >= 0; o--)
		{
			for(i=root;i>=0;i--)
			{
				lCh = (2*i)+1;
				rCh = (2*i)+2;
				if((lCh <= arr2) && (rCh <= arr2))
				{
					
					if(arr[rCh].getServicios().size()> arr[lCh].getServicios().size())
					{
						mCh = rCh;
					}
					else
					{
						mCh = lCh;
					}
				}
				else
				{
					if(rCh > arr2)
					{
						mCh = lCh;
					}
					else
					{
						mCh = rCh;
					}
				}

				if(arr[rCh].getServicios().size()< arr[lCh].getServicios().size())
				{
					tmp = arr[i];
					arr[i] = arr[mCh];
					arr[mCh] = tmp;
				}
			}
		}
		tmp = arr[0];
		arr[0] = arr[arr2];
		arr[arr2] = tmp;
		return;
	}


	/**
	 *  <br>retorna los servicios en un rango de tiempo
	 * <b> post: </b> retorna una cola en orden cronologico con servicios de taxi que comenzaron y terminaron dentro del tiempo pasado por parametro .
	 * @param rango = rango de fecha y hora en el cual se hara la busqueda.
	 * @throws ParseException 
	 * @throws IndexOutOfBoundsException 
	 * @throws Exception 
	 */
	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango) throws IndexOutOfBoundsException, ParseException
	{
		DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
		MyQueue<Servicio> serviciosPeriodo= new MyQueue<Servicio>();

		for (int i =0; i< services.size() ; i ++)
		{
			try {

				int com=services.getI(i).getFechaInicio().compareTo(formatoFecha.parse(rango.getFechaInicial()+'T'+ rango.getHoraInicio()) );

				int com2=services.getI(i).getFechaFinal().compareTo(formatoFecha.parse(rango.getFechaFinal()+'T'+rango.getHoraFinal()) );
				if (com>=0 && com2<=0)
				{
					serviciosPeriodo.enqueue(services.getI(i));
				}
			}
			//				quicksort(lista, 0, serviciosPeriodo.size());


			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return serviciosPeriodo;
		// TODO Auto-generated method stub
	}


	/**
	 *  <br>retorna los servicios en un rango de tiempo
	 * <b> post: </b> retorna una cola en orden cronologico con servicios de taxi que comenzaron y terminaron dentro del tiempo pasado por parametro .
	 * @param rango = rango de fecha y hora en el cual se hara la busqueda.
	 * @throws ParseException 
	 * @throws IndexOutOfBoundsException 
	 * @throws Exception 
	 */

	public IQueue <Servicio> darServiciosComienzanEnPeriodo(RangoFechaHora rango) throws IndexOutOfBoundsException, ParseException
	{
		MyQueue<Servicio> serviciosComienzanenPeriodo=new MyQueue<Servicio>();

		for (int i =0; i< services.size() ; i ++)
		{	
			try {

				int com=services.getI(i).getFechaInicio().compareTo(formatoFecha.parse(rango.getFechaInicial()+'T'+rango.getHoraInicio()) );
				int com2=services.getI(i).getFechaInicio().compareTo(formatoFecha.parse(rango.getFechaFinal()+'T'+rango.getHoraFinal()) );
				if (com>=0 && com2<=0)
				{
					serviciosComienzanenPeriodo.enqueue(services.getI(i));

				}

			}
			catch (Exception e) {
				// TODO: handle exception
				e.getMessage();
			}
		}
		return serviciosComienzanenPeriodo;
		// TODO Auto-generated method stub

	}
	/**
	 * @throws java.text.ParseException 
	 *  <br>devuelve el taxi con mas carreraas iniciadas en un rango de tiempo dado, de una compa�ia especifica
	 * <b> post: </b> devuelve el taxi de la compa�ia dada con mas servicios iniciados en el rango de tiempo.
	 * @param copany = nombre de la compa�ia .
	 * @param rango = rango de fecha y hora en el que se va a realizar la busqueda.
	 * @throws  
	 */

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company) throws ParseException
	{
		int carreras=0;
		int carreras2=0;
		Taxi resp=null;
		MyLinkedList<Servicio> servCom = new MyLinkedList<Servicio>();

		//		MyLinkedList<Taxi> taxisEnCompa�ia = new MyLinkedList<Taxi>();
		IQueue<Servicio> serviciosEnRango= darServiciosEnPeriodo(rango);


		for(int i =0; i< serviciosEnRango.size(); i++)
		{
			;
			Servicio x= serviciosEnRango.dequeue();
			if( x.getTaxiServicio().getCompany().equals(company))
			{

				servCom.add(x);	
			}
		}
		for(int i =0; i< servCom.size() ; i++) {
			String idTemp=servCom.getI(i).getTaxiServicio().gettaxi_id();
			System.out.println("llegue 2");
			for(int j=0 ; j< servCom .size() ; j++){
				String idTemp2=servCom.getI(j).getTaxiServicio().gettaxi_id();

				if(idTemp.equals(idTemp2)){
					carreras2++;
				}
			}

			if(carreras2>carreras){
				carreras=carreras2;
				resp=servCom.getI(i).getTaxiServicio();
				System.out.println("llegue 3");
			}
		}
		System.out.println("llegue 6");
		// TODO Auto-generated method stub
		return resp;
	}
	/**
	 *  <br>busca la informacion completa de un taxi 
	 * <b> post: </b> devuelve un objeto de tipo infoTaxiRango, con la informacion del taxi, nombre de la compa�ia, valor total ganado,numero  servicios prestados,distancia recorrida, tiempo total de servicio .
	 * @param id = id del taxi buscado .
	 * @param rango = rango de fecha y hora en el que se va a realizar la busqueda.
	 */
	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub

		double plata=0;
		String compa�ia="independiente ";
		double distanciaTotal=0;
		double tiempoTotal=0;
		MyLinkedList<Servicio> serv= new MyLinkedList<Servicio>();

		IQueue<Servicio> serviciosPeriodo= darServiciosEnPeriodo(rango);

		InfoTaxiRango info=null;
		for(int i=0; i<serviciosPeriodo.size(); i++ ){
			Servicio x= serviciosPeriodo.dequeue();

			if(x.getTaxiServicio().gettaxi_id().equals(id)){
				serv.add(x);
				plata= plata + x.getTrp_total();
				distanciaTotal= distanciaTotal+ x.getTripMiles();
				System.out.println(x.getTripMiles());
				tiempoTotal= tiempoTotal+ x.getTripSeconds();
				compa�ia= x.getTaxiServicio().getCompany();


				info= new InfoTaxiRango(id, rango, compa�ia, plata, serv, distanciaTotal,tiempoTotal);
			}}

		return info;
	}

	public double[] darDineroYDistancia(String id)
	{
		double[] dineroYDistancia= new double[1];

		//		MyLinkedList<Servicio> serv= new MyLinkedList<Servicio>();
		//		double dinero =0;
		//		double distancia=0;
		//		dineroYDistancia[0] =dinero;
		//		dineroYDistancia[1] =distancia;
		//
		//		for(int i=0; i<services.size(); i++ ){
		//			Servicio x= services.getI(i);
		//			if(x.gettaxi_id().equals(id)){
		//				serv.add(x);
		//				dinero=dinero+x.getTrp_total();
		//				distancia =distancia+ x.getTripMiles();
		//			}
		//		}

		return dineroYDistancia;
	}
	/**
	 * Retornar una lista de rangos de distancia recorrida, en la que se encuentran todos los
	 * servicios de taxis servidos por las compa��as, en una fecha dada y en un rango de horas
	 * especificada. La informaci�n debe estar ordenada por la distancia recorrida, as� la primera 
	 * posici�n de la lista tiene a su vez una lista con todos los servicios cuya distancia recorrida
	 * esta entre [0 y 1) milla. En la segunda posici�n, los recorridos entre [1 y 2) millas, y as�
	 * sucesivamente.
	 */

	@Override //4A
	public MyLinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		MyLinkedList<RangoDistancia> lisDistancia = new MyLinkedList<RangoDistancia>();

		int millas=0;

		RangoFechaHora rango = new  RangoFechaHora(fecha, horaInicial, fecha, horaFinal);

		IQueue<Servicio> colaServicios = darServiciosEnPeriodo(rango);


		MyLinkedList<Servicio> serviciosRango= new MyLinkedList<Servicio>();

		for(int i=0; i< colaServicios.size(); i++){

			millas =i+1;

			Servicio x= colaServicios.dequeue();
			while(x!=null)
			{
				if(x.getTripMiles()< millas && x.getTripMiles()>= i )
				{
					serviciosRango.add(x);
				}
				x= colaServicios.dequeue();
			}


			RangoDistancia rangoDist= new RangoDistancia(millas, i, serviciosRango);
			lisDistancia.add(rangoDist);

		}

		// TODO Auto-generated method stub
		return lisDistancia;
	} 

	@Override //1B
	public MyLinkedList<Compania> darCompaniasTaxisInscritos() {
		// TODO Auto-generated method stub
		MyLinkedList<Compania> retornar= new MyLinkedList<Compania>();

		int nComp=0;
		int c=0;

		while (c<compa�ia.size())
		{
			if(compa�ia.getI(c).getTaxisInscritos().size()>0)
			{
				retornar.add(compa�ia.getI(c));
			}
			c++;
		}
		Compania[] lista= new Compania[retornar.size()];

		mergeSortCompanias(lista);

		return retornar;

	}

	/**                                                                                                                       
	 *  <br> busca el taxi con mayor facturacion en un periodo de tiempo en una comp                                                                                                              
	 * <b> post: </b> taxi con mayor facturacion en la compa�ia dada
	 * @param rango = objeto de tipo RangoFecha , con la hora y fecha inicial y final donde se realizara la busqueda .                                                                  
	 * @param nomCompania= nombre de la compa�ia del taxi.                                                          
	 */ 
	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		double carreras2=0;

		Taxi retornar=null;
		//		IQueue<Servicio> serviciosEnRango= darServiciosEnPeriodo(rango);
		//		for(int i =0; i< serviciosEnRango.size(); i++)
		//		{
		//			Servicio x= serviciosEnRango.dequeue();
		//			double carreras=x.getTrp_total();
		//			for(int y=0; y< serviciosEnRango.size();  y ++ )
		//			{
		//				Servicio d=	serviciosEnRango.dequeue();
		//				if(x.gettaxi_id().equals(d.gettaxi_id()))
		//				{
		//					carreras+= d.getTrp_total();
		//					serviciosEnRango.enqueue(d);
		//				}
		//				else
		//				{
		//					serviciosEnRango.enqueue(d);
		//				}
		//				if(carreras> carreras2)
		//				{
		//					retornar= x.getTaxiServicio();
		//					carreras2= carreras;
		//				}
		//			}
		//		}

		// TODO Auto-generated method stub
		return retornar;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{


		ServiciosValorPagado[] retornar =new ServiciosValorPagado[3];

		// cuando inicia en la zona que se pasa por par�metro pero termina en otra
		double totalIniciados=0;
		MyLinkedList<Servicio> lIn= new MyLinkedList<Servicio>();
		ServiciosValorPagado in= new ServiciosValorPagado(lIn, totalIniciados);

		// cuando no inicia en la zona que se pasa por par�metro pero s� termina en ella
		double totalTerminados=0;
		MyLinkedList<Servicio> lTer= new MyLinkedList<Servicio>();
		ServiciosValorPagado ter= new ServiciosValorPagado(lTer, totalTerminados);

		// cuando inicia en la zona que se pasa por par�metro y  termina en esta
		double totalInTerm=0;
		MyLinkedList<Servicio> lInyTer= new MyLinkedList<Servicio>();
		ServiciosValorPagado inyTer= new ServiciosValorPagado(lInyTer, totalInTerm);

		IQueue<Servicio> serviciosEnRango= darServiciosEnPeriodo(rango);
		while(serviciosEnRango.size()>0)
		{
			Servicio x= serviciosEnRango.dequeue();
			if(x.getId_zonaInicial().equals(idZona)&& !x.getId_zonaFinal().equals(idZona))
			{
				lIn.add(x);
				totalIniciados+= x.getTrp_total();
				in.setValorAcumulado(totalIniciados);
				in.setServiciosAsociados(lIn);
			}
			else if(!x.getId_zonaInicial().equals(idZona)&& x.getId_zonaFinal().equals(idZona))
			{
				lTer.add(x);
				totalTerminados+= x.getTrp_total();
				ter.setValorAcumulado(totalIniciados);
				ter.setServiciosAsociados(lTer);
			}
			else if(x.getId_zonaInicial().equals(idZona)&& x.getId_zonaFinal().equals(idZona))
			{
				lInyTer.add(x);
				totalInTerm+= x.getTrp_total();
				inyTer.setServiciosAsociados(lInyTer);
				inyTer.setValorAcumulado(totalInTerm);
			}

		}
		retornar[0]= in;
		retornar[1]= ter;
		retornar[2]= inyTer;

		// TODO Auto-generated method stub
		return retornar;
	}
	/**                                                                                                                                                                                          
	 *  <br> busca la informacion completa de una zona de la ciudad en un periodo de tiempo                                                                                                              
	 * <b> post: </b> lista de objetos tipo Zona de servicios con: id de la zona y fecha de servicio                                                                                                                              
	 * @param rango = objeto de tipo RangoFecha , con la hora y fecha inicial y final donde se realizara la busqueda .                                                                            
	 */                                                                                                                                                                                           

	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		MyLinkedList<ZonaServicios> retornar = new MyLinkedList<ZonaServicios>();
		ZonaServicios[] z= new ZonaServicios[zonaServicio.size()];

		for(int i=0; i<zonaServicio.size(); i++)
		{

			for(int j=0; j<zonaServicio.getI(i).getFechasServicios().size();j++)
			{
				zonaServicio.getI(i).getFechasServicios().getI(j).setServiciosAsociados(darServiciosComienzanEnPeriodoLista(rango, zonaServicio.getI(i).getFechasServicios().getI(j).getServiciosAsociados()));		
				FechaServicios[] f= new FechaServicios[zonaServicio.getI(i).getFechasServicios().size()];
				f[j]= zonaServicio.getI(i).getFechasServicios().getI(j);
				mergeSortFechaServicios(f);
				zonaServicio.getI(i).getFechasServicios().getI(j).setServiciosAsociados((LinkedList<Servicio>) Arrays.asList(f));		
			}

			z[i]= zonaServicio.getI(i);

		}
		mergeSortZonaServicios(z);
		retornar= (MyLinkedList<ZonaServicios>) Arrays.asList(z);
		// TODO Auto-generated method stub
		return retornar;
	}

	/**                                                                                                                         
	 *  <br> busca el taxi mas rentable de cada compa�ia es decir, el taxi culla rellacion de dinero ganado por distancia rcrrida es mayor                              
	 * <b> post: </b> lista con los taxis mas rentables de las compa�ias                       
	 */


	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		MyLinkedList<CompaniaTaxi> taxisRentables = new MyLinkedList<CompaniaTaxi>();
		CompaniaTaxi resp=null;
		Taxi taxiRent= null;
		double relacion=0;
		double relacion2=0;
		for (int i=0; i<compa�ia.size(); i++){

			LinkedList<Taxi> taxisInscritos = compa�ia.getI(i).getTaxisInscritos();

			for(int j=0; j<taxisInscritos.size(); i++)
			{

				double[] dineroYDistancia=darDineroYDistancia(taxisInscritos.getI(j).gettaxi_id());

				relacion2= dineroYDistancia[0]/ dineroYDistancia[1];

				if(relacion2>relacion)
				{
					relacion =relacion2;
					taxiRent= taxisInscritos.getI(j);
				}

			}
			resp= new CompaniaTaxi(compa�ia.getI(i).getNombre(),taxiRent );
			taxisRentables.add(resp);
		}
		// TODO Auto-generated method stub
		return taxisRentables;
	}

	//4C
	/**
	 * Dada la gran cantidad de datos que requiere el proyecto, se desea poder compactar
	 * informaci�n asociada a un taxi particular. Para ello usted debe guardar en una pila todos
	 * los servicios generados por el taxi en orden cronol�gico, entre una hora inicial y una hora
	 * final, en una fecha determinada.
	 */
	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		MyStack<Servicio> serviciosTaxi= new MyStack<Servicio>();
		RangoFechaHora rango = new RangoFechaHora(fecha,horaInicial,fecha, 
				horaFinal);
		InfoTaxiRango info = darInformacionTaxiEnRango(taxiId, rango);

		MyLinkedList< Servicio> ser = info.getServiciosPrestadosEnRango();
		for (int i = 0 ; i< ser.size() ; i++){
			{
				serviciosTaxi.push(ser.getI(i));
			}
		}
		// TODO Auto-generated method stub
		return serviciosTaxi;
	}
	private Date parseRangoFechaCompleto(String f)
	{
		SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		Date fecha2 = null;
		try {


			fecha2= (Date)formato.parse(f);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fecha2;
	}
	private Date parseRangoFecha(String f, String h)
	{
		SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		Date fecha2 = null;
		try {

			String fh= f+ "T" + h;
			fecha2= (Date)formato.parse(fh);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fecha2;
	}
	private void mergeSortCompanias(Compania list[])
	{

		if (list.length > 1) {
			// Merge sort the first half
			Compania[] firstHalf = new Compania[list.length / 2];
			System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
			mergeSortCompanias(firstHalf);

			// Merge sort the second half
			int secondHalfLength = list.length - list.length / 2;
			Compania[] secondHalf = new Compania[secondHalfLength];
			System.arraycopy(list, list.length / 2,
					secondHalf, 0, secondHalfLength);
			mergeSortCompanias(secondHalf);

			// Merge firstHalf with secondHalf into list
			mergeCompanias(firstHalf, secondHalf, list);

		}
	}
	private static void mergeCompanias(Compania[] list1, Compania[] list2, Compania[] temp) {
		int current1 = 0; // Current index in list1
		int current2 = 0; // Current index in list2
		int current3 = 0; // Current index in temp

		while (current1 < list1.length && current2 < list2.length) {
			if (list1[current1].getNombre().compareToIgnoreCase(list2[current2].getNombre())<0)
				temp[current3++] = list1[current1++];
			else
				temp[current3++] = list2[current2++];
		}

		while (current1 < list1.length)
			temp[current3++] = list1[current1++];

		while (current2 < list2.length)
			temp[current3++] = list2[current2++];
	}


	private void mergeSortCompaniasServicios(CompaniaServicios list[])
	{

		if (list.length > 1) {
			// Merge sort the first half
			CompaniaServicios[] firstHalf = new CompaniaServicios[list.length / 2];
			System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
			mergeSortCompaniasServicios(firstHalf);

			// Merge sort the second half
			int secondHalfLength = list.length - list.length / 2;
			CompaniaServicios[] secondHalf = new CompaniaServicios[secondHalfLength];
			System.arraycopy(list, list.length / 2,
					secondHalf, 0, secondHalfLength);
			mergeSortCompaniasServicios(secondHalf);

			// Merge firstHalf with secondHalf into list
			mergeCompaniasServicios(firstHalf, secondHalf, list);

		}
	}

	private static void mergeCompaniasServicios(CompaniaServicios[] list1, CompaniaServicios[] list2, CompaniaServicios[] temp) {
		int current1 = 0; // Current index in list1
		int current2 = 0; // Current index in list2
		int current3 = 0; // Current index in temp

		while (current1 < list1.length && current2 < list2.length) {
			if (list1[current1].getServicios().size()>list2[current2].getServicios().size())
				temp[current3++] = list1[current1++];
			else
				temp[current3++] = list2[current2++];
		}

		while (current1 < list1.length)
			temp[current3++] = list1[current1++];

		while (current2 < list2.length)
			temp[current3++] = list2[current2++];
	}
	public static void quicksort(Servicio A[], int izq, int der) {

		Servicio pivote=A[izq]; // tomamos primer elemento como pivote
		int i=izq; // i realiza la b�squeda de izquierda a derecha
		int j=der; // j realiza la b�squeda de derecha a izquierda
		Servicio aux;

		while(i<j){            // mientras no se crucen las b�squedas
			while(A[i].getFechaInicio().compareTo(pivote.getFechaInicio())<=0 && i<j) i++; // busca elemento mayor que pivote
			while(A[j].getFechaInicio().compareTo(pivote.getFechaInicio())>0) j--;         // busca elemento menor que pivote
			if (i<j) {                      // si no se han cruzado                      
				aux= A[i];                  // los intercambia
				A[i]=A[j];
				A[j]=aux;
			}
		}
		A[izq]=A[j]; // se coloca el pivote en su lugar de forma que tendremos
		A[j]=pivote; // los menores a su izquierda y los mayores a su derecha
		if(izq<j-1)
			quicksort(A,izq,j-1); // ordenamos subarray izquierdo
		if(j+1 <der)
			quicksort(A,j+1,der); // ordenamos subarray derecho
	}
	private void mergeSortFechaServicios(FechaServicios list[])
	{

		if (list.length > 1) {
			// Merge sort the first half
			FechaServicios[] firstHalf = new FechaServicios[list.length / 2];
			System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
			mergeSortFechaServicios(firstHalf);

			// Merge sort the second half
			int secondHalfLength = list.length - list.length / 2;
			FechaServicios[] secondHalf = new FechaServicios[secondHalfLength];
			System.arraycopy(list, list.length / 2,
					secondHalf, 0, secondHalfLength);
			mergeSortFechaServicios(secondHalf);

			// Merge firstHalf with secondHalf into list
			mergeFechaServicios(firstHalf, secondHalf, list);

		}
	}
	private static void mergeFechaServicios(FechaServicios[] list1, FechaServicios[] list2, FechaServicios[] temp) {
		int current1 = 0; // Current index in list1
		int current2 = 0; // Current index in list2
		int current3 = 0; // Current index in temp

		while (current1 < list1.length && current2 < list2.length) {

			if (list1[current1].getFecha().compareToIgnoreCase(list2[current2].getFecha())<0)
				temp[current3++] = list1[current1++];
			else
				temp[current3++] = list2[current2++];
		}

		while (current1 < list1.length)
			temp[current3++] = list1[current1++];

		while (current2 < list2.length)
			temp[current3++] = list2[current2++];
	}

	private void mergeSortZonaServicios(ZonaServicios list[])
	{

		if (list.length > 1) {
			// Merge sort the first half
			ZonaServicios[] firstHalf = new ZonaServicios[list.length / 2];
			System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
			mergeSortZonaServicios(firstHalf);

			// Merge sort the second half
			int secondHalfLength = list.length - list.length / 2;
			ZonaServicios[] secondHalf = new ZonaServicios[secondHalfLength];
			System.arraycopy(list, list.length / 2,
					secondHalf, 0, secondHalfLength);
			mergeSortZonaServicios(secondHalf);

			// Merge firstHalf with secondHalf into list
			mergeZonaServicios(firstHalf, secondHalf, list);

		}
	}
	private static void mergeZonaServicios(ZonaServicios[] list1, ZonaServicios[] list2, ZonaServicios[] temp) {
		int current1 = 0; // Current index in list1
		int current2 = 0; // Current index in list2
		int current3 = 0; // Current index in temp

		while (current1 < list1.length && current2 < list2.length) {
			if (list1[current1].getIdZona().compareToIgnoreCase(list2[current2].getIdZona())<0)
				temp[current3++] = list1[current1++];
			else
				temp[current3++] = list2[current2++];
		}

		while (current1 < list1.length)
			temp[current3++] = list1[current1++];

		while (current2 < list2.length)
			temp[current3++] = list2[current2++];
	}
	private LinkedList <Servicio> darServiciosComienzanEnPeriodoLista(RangoFechaHora rango, LinkedList<Servicio> list) throws IndexOutOfBoundsException, ParseException
	{
		MyLinkedList<Servicio> serviciosComienzanenPeriodo=new MyLinkedList<Servicio>();
		DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
		for (int i =0; i< list.size() ; i ++)
		{	
			try {

				int com=list.getI(i).getFechaInicio().compareTo(parseRangoFecha(rango.getFechaInicial(),rango.getHoraInicio()) );
				int com2=list.getI(i).getFechaInicio().compareTo(parseRangoFecha(rango.getFechaFinal(),rango.getHoraFinal()) );
				if (com>=0 && com2<=0)
				{
					serviciosComienzanenPeriodo.add(list.getI(i));

				}

			}
			catch (Exception e) {
				// TODO: handle exception
				e.getMessage();
			}
		}
		return serviciosComienzanenPeriodo;
		// TODO Auto-generated method stub

	}

	@Override
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n) {
		// TODO Auto-generated method stub
		return null;
	}

}
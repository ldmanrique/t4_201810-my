package model.data_structures;

public interface IMaxPQ < Key >  {


	void MaxPQ();// create a priority queue
	void MaxPQ(int max) ;//create a priority queue of initial capacity max
	void MaxPQ(Key[] a); // create a priority queue from the keys in a[]
	void insert(Key v); // insert a key into the priority queue
	Key max() ; //return the largest key
	Key delMax(); // return and remove the largest key
	boolean isEmpty();// is the priority queue empty?
	int size(); // number of keys in the priority queue

}

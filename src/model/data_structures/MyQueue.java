package model.data_structures;

public class MyQueue<T extends Comparable<T>> implements IQueue<T> {
	private Node<T> first;
	private Node<T> last;
	private int size = 0;

	@Override
	/**
	 * agrega un elemento al final de la cola
	 */
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		Node <T> newNode = new Node<T> (item);
		if (size == 0) {
		first = newNode;
		last = newNode;
		}
		else {
		Node <T> oldLast = last;
		oldLast.setNextNode(newNode);
		last = newNode;
		}
		size++;

	}
	/**
	 * saca el primer elemento de la cola
	 */

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
//		if (size == 0)
//			throw new EmptyQueueException();
			Node <T> oldFirst = first;
			T elem = first.getItem();
			first = oldFirst.getNext();
			oldFirst.setNextNode(null);
			size--;
			return elem;

	}
	/**
	 * dice si esta vacia la cola
	 */

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size==0);
	}
/**
 * retorna el tama�o de la cola
 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	

}

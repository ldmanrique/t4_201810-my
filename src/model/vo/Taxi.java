package model.vo;

import model.data_structures.LinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{

	private String company;
	private String taxi_id;
	private int nServicios;
	private LinkedList<Servicio> servicios;
	
	
	public Taxi(String c, String id /** int nServi*/)
	{
		company=c;
		taxi_id=id;
		//setnServicios(nServi);
	}
	/**
	 * @return id - taxi_id
	 */
	public String gettaxi_id() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public void setCompany( String name)
	{
		company= name;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public int getnServicios() {
		return nServicios;
	}
	public void setnServicios(int nServicios) {
		this.nServicios = nServicios;
	}
	public LinkedList<Servicio> getServicios() {
		return servicios;
	}
	public void setServicios(LinkedList<Servicio> servicios) {
		this.servicios = servicios;
	}	
}

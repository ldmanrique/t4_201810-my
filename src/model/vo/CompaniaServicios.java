package model.vo;

import model.data_structures.LinkedList;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String company;
	
	private LinkedList<Servicio> servicios;

	public CompaniaServicios(String pNombre, LinkedList<Servicio> pServicios)
	{
		company= pNombre;
	   servicios=pServicios;
	}

	public String getcompany() {
		return company;
	}

	public void setcompany(String company) {
		this.company = company;
	}

	public LinkedList<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(LinkedList<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
